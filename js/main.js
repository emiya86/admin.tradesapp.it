jQuery.noConflict();
(function( $ ) {
  $(function() {
    /*PROVVISORIO */
    $( document ).ready(function() {
      //$("#impostazioni").trigger("click");
    });

/*MENU:apri/chiudi sottomenu*/
$( ".metismenu li.main" ).click(function() {
  if($(this).attr("class")!="active" && $(this).attr("class")!="main active" && $(window).width() > 1024){
    $("li.active ul.mm-collapse").css('opacity', 1).slideUp('slow').animate({ opacity: 0 },{ queue: false, duration: 'slow' });
    $(".metismenu li").removeClass("active");  
    $(this).addClass("active");
    $(".active ").find("ul.mm-collapse").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' });
  }
 else if($(window).width() <= 1024){
    if($(this).attr("class")!="active" && $(this).attr("class")!="main active"){
      $(this).addClass("active");
      $(".active ").find("ul.mm-collapse").css('opacity', 0).slideDown('slow').animate({ opacity: 1 },{ queue: false, duration: 'slow' });
    }
    else{
      $("li.active ul.mm-collapse").css('opacity', 1).slideUp('slow').animate({ opacity: 0 },{ queue: false, duration: 'slow' });
      $(".metismenu li").removeClass("active");
    }
}
});
/*MENU: carica pagine*/
$( ".metismenu .nav-text" ).click(function() {
$( ".content_fluid" ).load( "class/"+$(this).attr("id")+".php", function() {
  var rbd=0;
  $("#chart_sommario .apexcharts-radialbar-area").each(function() {
    if($(this).parent().attr("seriesName")!=undefined){
      $("svg.radiantbar-"+rbd).css("color",$(this).attr("stroke"));
      rbd++;
    }
  });
}).show("slow");
});
/*MENU: Menu mobile */
$( ".navigation li.list" ).click(function() {
$( ".navigation li.list" ).removeClass("active");
$(this).addClass("active");
$("#"+$(this).attr("id")).trigger("click");
scroll_to("root");
});
/*ADD USER: controllo form */
$("#root").on("click", "button#adduser", function(){
  var u=$("#user").val();
  var e=$("#email").val();
  var s=$("#scadenza").val();
  if(u=='' || e=='' || e.indexOf("@")==-1 || e.length<4){
    $(".statusform").html("<div class=\"error bg-danger\"><p>Controlla username o email.</p></div>").hide().show("slow").delay(4000).hide("slow");
  }else{
    $.post("class/db/db_user.php", {b:"add",u:u,e:e,s:s},function(dt) {$(".statusform").html(dt).hide().show("slow").delay(4000).hide("slow");});  
  }
});
/*ADD USER: delete user*/
$("#root").on("click", ".deltrans svg", function(){
  var u=$(this).closest(".buser").attr("id");
$(".confirmdelete").show("slow").attr("duser",u);

});
$("#root").on("click", ".confirmdelete button", function(){
  if($(this).text()=="Si"){
    var u=$(".confirmdelete").attr("duser");
    $.post("class/db/db_user.php", {b:"delete",u:u},function(dt) {console.log(dt);});
  }
  $(".confirmdelete").hide("slow");
  });


  });

})(jQuery);


