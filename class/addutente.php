<?php
require('utility.php');
echo titolopagina('Aggiungi nuovo utente').add_user();
function add_user(){
    return '
<div class="row add_user">
    <div class="card">
        <div class="row duser">
            <div class="col-4">
                <h4>Username</h4>
                <input class="form-control" type="text" name="user" id="user"></input>
            </div>
            <div class="col-4">
                <h4>Email</h4>
                <input class="form-control" type="email" name="email" id="email"></input>
            </div>
            <div class="col-4">
                <h4>Scadenza</h4>
                <input class="form-control" type="date" name="scadenza" id="scadenza"></input>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="statusform"></div>
            </div>
        </div>
        <div class="row auser">
            <div class="col-3 offset-9">
                <button class="btn btn-primary" id="adduser">Aggiungi</button>
            </div>
        </div>
        </div>
    </div>
</div>    
    ';
}
?>