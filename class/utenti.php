<?php
require('utility.php');
echo titolopagina('Elenco degli utenti').tab_utenti();

function tab_utenti(){
    include('db/db_user.php');
    $d=users();
    return '
<div class="row utenti">
    <div class="card">
        <div class="card-header">'.cerca(count($d)).'</div>
        <div class="card-body">'.elenco_user($d).'</div>
        <div class="confirmdelete">
            <div class="card">
                <h4>Sei sicuro di voler eliminare l\'utente?</h4>
                <div class="row">
                    <div class="col-6"> <button class="btn btn-primary">Si</button></div>
                    <div class="col-6"><button class="btn btn-secondary">No</button></div>
                </div>
            </div>
        </div>
    </div>    
</div>';
}
function cerca($u){
return '

<div class="row filtri_tabella text-center">
    <div class="col-2">
        <h4>Username:</h4>
        <input class="form-control input-search" type="text" name="username" id="username">
    </div>
    <div class="col-2">
        <h4>Email:</h4>
        <input class="form-control input-search" type="email" name="email" id="email">
    </div>
    <div class="col-2">
        <h4>Fuso Orario:</h4>
        <input class="form-control input-search" type="text" name="fuso" id="fuso">
    </div>
    <div class="col-2">
        <h4>Regole email:</h4>
        <select class="form-control input-search" name="regole" id="regole"><option value="">Scegli un\'opzione</option><option value="si">Si</option><option value="no">No</option></select>
    </div>
    <div class="col-2">
        <h4>Data Scadenza:</h4>
        <input class="form-control input-search" type="date" name="scadenza" id="scadenza">
    </div>
    <div class="col-2">
        <h4>Utenti totali:</h4>
        <p>'.$u.'</p>
    </div>    
</div>
';
}
function elenco_user($d){
    $t='';
    if(is_array($d)){
        $i=0;
        $bk=['#fff','#eee'];
        foreach ($d as $v){
            $t.='<div class="row buser text-center" style="background-color:'.($i & 1? $bk[0]:$bk[1]).';" id="'.$v['username'].'">
                <div class="col-2">'.$v['username'].'</div>
                <div class="col-2">'.$v['email'].'</div>
                <div class="col-2">'.$v['fusoorario'].'</div>
                <div class="col-2">'.(strlen($v['erules'])>1? 'Si' : 'No').'</div>
                <div class="col-2">'.$v['scadenza'].'</div>
                <div class="col-2 deltrans">'.icone('remove').'</div>
            </div>  ';
            $i++;
        }
    }
    return $t;
} 
?>