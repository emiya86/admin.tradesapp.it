<?php
include ('db.php');
if(isset($_POST['b']) && $_POST['b']=='add'){
    echo add_user($_POST['u'],$_POST['e'],$_POST['s']);
}else if(isset($_POST['b']) && $_POST['b']=='delete'){
    delete_user($_POST['u']);
    echo tab_utenti();
}
function users(){
    $db=OpenCon();
    $q='SELECT username,email,fusoorario,erules,scadenza FROM user_data;';
    $r=$db->query($q);
    if ($r!== false) {
        $d=[];
        $i=0;
        if($r->num_rows > 0) {
            while($row = $r->fetch_assoc()) {
                $d[$i]=$row;
                $i++;
            }
        }
        
        return $d;
    }
    else{return '';}
    CloseCon($db);
}
function add_user($u,$e,$s){
    $db=OpenCon();
    $q='SELECT * FROM user_data WHERE username="'.$u.'" LIMIT 1;';
    if(check_db($db,$q)==true){CloseCon($db);return '<div class="error bg-danger"><p>L\'username esiste già. Scegline uno diverso</p></div>';}
    $q='SELECT * FROM user_data WHERE email="'.$e.'" LIMIT 1;';
    if(check_db($db,$q)==true){CloseCon($db);return '<div class="error bg-danger"><p>L\'email esiste già. Scegline uno diverso</p></div>';}
    $q='INSERT INTO user_data (unhash,username,email,pwd,scadenza) VALUES ("'.hash('sha256',$u).'","'.$u.'","'.$e.'","'.hash('sha256',"000000").'","'.$s.'");';
    $db->query($q);
    create_table($u,$db);
    CloseCon($db);
    return '<div class="error bg-success"><p>Accont aggiunto correttamente.</p></div>';

}
function check_db($db,$q){
    $r=$db->query($q);
    if ($r!== false) {
        if($r->num_rows > 0) {
            return true;
        }
    }
}
function create_table($u,$db){
    $t=[
        '_hold'=>'(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,idstock VARCHAR (255) NULL,pos VARCHAR(255) NULL,price FLOAT NULL,qty INT NULL,margin INT NULL,commission INT NULL);',
        '_list_mov'=>'(n INT NOT NULL AUTO_INCREMENT PRIMARY KEY,data DATE NULL,importo FLOAT NULL,descrizione VARCHAR(255) NULL,categoria VARCHAR(255) NULL,cassa VARCHAR(255) NULL);',
        '_mov'=>'(id VARCHAR (255) PRIMARY KEY,cn VARCHAR(255) NULL,ca VARCHAR(255) NULL);',
        '_price'=>'(id VAR CHART (255) NULL,price VARCHAR(255) NULL);',
        '_rules'=>'(id VARCHAR (255) PRIMARY KEY,status VARCHAR(255) NULL,rule VARCHAR(255) NULL,mov VARCHAR(255) NULL,wcheck VARCHAR(255) NULL,word VARCHAR(255) NULL,action VARCHAR(255) NULL);',
        '_stat'=>'(id INT AUTO_INCREMENT PRIMARY KEY,idstock VARCHAR (255) NULL,stock VARCHAR (255) NULL,market VARCHAR (255) NULL,buyprice FLOAT NULL,sellprice FLOAT NULL,qty INT NULL,pos VARCHAR(255) NULL,time DATE NULL,valuta VARCHAR(255) NULL);',
        '_tdg'=>'(id VARCHAR (255) PRIMARY KEY,stock VARCHAR (255) NULL,stockname VARCHAR(255) NULL,market VARCHAR(255) NULL,valuta VARCHAR(255) NULL);'
    ];
    foreach ($t as $k=>$v){
        $q='SHOW TABLES LIKE '.$u.$k;
        $r=$db->query($q);
        if($r=== false) {
            $q='CREATE TABLE '.$u.$k.' '.$v;
            $db->query($q);
        }
    }
}
function delete_user($u){
    $db=OpenCon();
    $q='DELETE FROM user_data WHERE username="'.$u.'";';
    $db->query($q);
    delete_table($u,$db);
    CloseCon($db);
}
function delete_table($u,$db){
    $t=['_hold','_list_mov','_mov','_price','_rules','_stat','_tdg'];
    foreach ($t as $v){
        $q='DROP TABLE '.$u.$v.';';
        $db->query($q);
    }
}
?>