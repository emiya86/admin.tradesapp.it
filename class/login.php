<?php
function loginadmin(){
    return '
    <div id="" class="mh100vh">
        <div>
            <div>
                <div class="authincation h-100 p-meddle">
                    <div class="container h-100">
                        <div class="row justify-content-center h-100 align-items-center">
                            <div class="col-md-6">
                                <form method="post" action="#">
                                    <div class="authincation-content">
                                        <div class="row no-gutters">
                                            <div class="col-xl-12">
                                                <div class="auth-form card">
                                                    <div class="logo row align-items-center">
                                                        <div class="col-4 col-sm-3 col-xl-2 offset-xl-3"><img src="https://my.tradesapp.it/img/lg_tradesapp.png" alt=""></div>
                                                        <div class="col-8 col-sm-9 col-xl-6"><h1>Tradesapp</h1></div>                                         
                                                    </div>
                                                    <div class="row loginadmin">
                                                        <div class="col-12"><h4 class="text-center">Area Riservata</h4></div>                                                 
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <label class=""><strong>Username</strong></label>
                                                                <input type="text" class="form-control" value="" name="email" id="email">
                                                            </div>
                                                            <div class="form-group">
                                                                <label class=""><strong>Password</strong></label>
                                                                <input type="password" class="form-control" value="" name="pwd" id="pwd">
                                                            </div>
                                                            <div class="text-center">
                                                                <button type="submit" id="loginb" class="btn btn-primary btn-block">Accedi</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<style>body{background:var(--color);}</style>
';    
}
function login(){
    include('dashboard.php');
    include('menu.php');
    include('header.php');
return '
<div id="root">
    <div class="root">
        <div class="left_menu d-none d-md-block">'.menu().'</div>
        <div class="body_container">
            '.testata().'
            <div class="content_body"><div class="content_fluid">'.dashboard().'</div></div>
        </div>
        <div class="navigation d-md-none">'.menu_mobile().'</div>
    </div>
</div>
';
}
?>